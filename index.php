<?php
    include_once(__DIR__ . "/controllers/home_controller.php");
    require_once(__DIR__ . "/models/home_model.php");

    $data = new HomeController();

    $page = empty($_POST) ? 'main' : $_POST['type'];
    switch ($page) {
        case 'main':
            $controller = new HomeController();
            $controller->showMainPage();
            break;
        case 'new':
            if (!empty($_POST['first_name']) && !empty($_POST['second_name']) && !empty($_POST['email'])) {
                $controller = new HomeController();
                $controller->showNewPage();
            }
            break;
        case 'delete':
            if (!empty($_POST['id'])){
                $controller = new HomeController();
                $controller->deleteItemPage($_POST['id']);
            }
            break;
        case 'update':
            if (!empty($_POST['id']) && !empty($_POST['first_name']) && !empty($_POST['second_name']) && !empty($_POST['email'])) {
                $controller = new HomeController();
                $controller->updateItemPage($_POST['id'], $_POST["first_name"], $_POST["second_name"], $_POST["email"]);
                $select = new HomeModel();
                $json = json_encode($select->selectOne($_POST['id']));
                echo $json;
            }
            break;
    }
