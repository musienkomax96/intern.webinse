<?php

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    include_once(__DIR__ . "/../views/home_view.php");
    include_once(__DIR__ . "/../models/home_model.php");

    class HomeController {
        public function showMainPage() {
            $home = new HomeModel();
            $mainView = new View();
            $mainView->render('main', $home->selectAll());
        }

        public function showNewPage() {
            $home = new HomeModel();
            $mainView = new View();
            $home->insert($_POST["first_name"], $_POST["second_name"], $_POST["email"]);
            $mainView->render('new', $home->selectLast());
        }

        public function deleteItemPage($id) {
            $del = new HomeModel();
            $del->delete($id);
        }

        public function updateItemPage($id, $firstName, $secondName, $email) {
            $upItem = new HomeModel();
            $upItem->update($id, $firstName, $secondName, $email);
        }
    }
