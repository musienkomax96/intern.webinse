<?php

    require_once 'dataBase_model.php';

    class HomeModel extends DataBase{

        function selectAll(){
            $result = mysqli_query($this->link, "SELECT * FROM `test_table`");
            $arr = array();
            while($row=mysqli_fetch_array($result)){
                array_push($arr, $row);
            }
            return $arr;
        }

        function selectOne($id){
            global $resultSelectOne;
            $r = mysqli_query($this->link, "SELECT * FROM `test_table` WHERE id='$id'");
            if ($r) {
                $resultSelectOne = mysqli_fetch_array($r);
            }
            return $resultSelectOne;
        }

        function selectLast(){
            $r = mysqli_query($this->link, "SELECT * FROM `test_table` ORDER BY `id` DESC LIMIT 1");
            if ($r) {
                $res = mysqli_fetch_array($r);
            }
            return $res;
        }

        function insert($first_name, $second_name, $email): bool{
            $result = mysqli_query($this->link, "INSERT INTO `test_table` (`id`, `first_name`, `second_name`, `email`) VALUES (NULL, '$first_name', '$second_name', '$email')");
            return $result;
        }

        function update($id, $new_first_name, $new_second_name, $new_email):bool {
            $result = mysqli_query($this->link, "UPDATE `test_table` SET `first_name`='$new_first_name', `second_name`='$new_second_name', `email`='$new_email' WHERE (`id`=$id)");
            return $result;
        }

        function delete($id): bool{
            $result = mysqli_query($this->link, "DELETE FROM `test_table` WHERE (id='$id')");
            return $result;
        }
    }
