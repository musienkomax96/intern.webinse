<?php
    class DataBase {
        private $db_host='localhost'; // ваш хост
        private $db_name='intern_webinse'; // ваша бд
        private $db_user_name='root'; // пользователь бд
        private $db_user_pwd='secret'; // пароль к бд

        protected $link;

        private function connectToDb($db_host, $db_name, $db_user_name, $db_user_pwd){
            $this->link = mysqli_connect($db_host, $db_user_name, $db_user_pwd, $db_name);
            return $this->link;
        }

        function __construct()
        {
            $this->connectToDb($this->db_host, $this->db_name, $this->db_user_name, $this->db_user_pwd);

            mysqli_query($this->link, "SET NAMES 'utf8'");
            mysqli_query($this->link, "SET CHARACTER SET 'utf8'");
            mysqli_query($this->link, "SET SESSION collation_connection = 'utf8_general_ci'");
        }
    }
