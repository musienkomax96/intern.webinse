<html>
<head>
    <title>
        Webinse test
    </title>
    <link rel="stylesheet" href="../src/css/bootstrap.min.css">
    <link rel="stylesheet" href="../src/css/styles_for_input_form.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>
<body>
<table class="table table-hover table-striped table-dark table-bordered">
    <thead>
    <tr>
        <th scope="col">id</th>
        <th scope="col">First name</th>
        <th scope="col">Second name</th>
        <th scope="col">E-mail</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($arr as $row): ?>
        <tr id="tr_<?= $row['id'] ?>">
            <th scope="row"><?= $row['id'] ?></th>
            <td scope="row"><?= $row['first_name'] ?></td>
            <td scope="row"><?= $row['second_name'] ?></td>
            <td scope="row"><?= $row['email'] ?></td>
            <td>
                <a href="#" id="num_<?= $row['id'] ?>" class="badge badge-light btn_update">update</a>
                <a href="#" id="num_<?= $row['id'] ?>" class="badge badge-light btn_delete">delete</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<a id="add_item" class="btn btn-dark text-light">Добавить запись</a>

<div id="add_container" class="container">
    <p>Добавление</p>
    <form method="POST" action="#" id="form_add" class="ajax">
        <div>
            <label for="first_name">First name</label>
            <input name="first_name" id="add_first_name" type="text"/><br>
        </div>
        <div>
            <label for="second_name">Second name</label>
            <input name="second_name" id="add_second_name" type="text"/><br>
        </div>
        <div>
            <label for="email">E-mail</label>
            <input name="email" id="add_email" type="text"/><br>
        </div>
        <div>
            <input class="btn btn-dark submit_add" type="submit" value="Отправить"/>
            <a class="btn btn-dark text-light cancel">Отмена</a>
        </div>
    </form>
</div>

<div id="update_container" class="container">
    <p>Редактирование</p>
    <form method="POST" action="#" id="form_update" class="ajax">
        <div>
            <label for="first_name">First name</label>
            <input name="first_name" id="update_first_name" type="text"/><br>
        </div>
        <div>
            <label for="second_name">Second name</label>
            <input name="second_name" id="update_second_name" type="text"/><br>
        </div>
        <div>
            <label for="email">E-mail</label>
            <input name="email" id="update_email" type="text"/><br>
        </div>
        <div>
            <input id="sub_btn_update" class="btn btn-dark" type="submit" value="Заменить"/>
            <a class="btn btn-dark text-light cancel">Отмена</a>
        </div>
    </form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!--              <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="../src/js/bootstrap.js"></script>
<script src="../src/js/home_controller.js"></script>
</body>
</html>
