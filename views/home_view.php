<?php

    class View {
        public function render($nameOfTemplate, $arr) {
            switch ($nameOfTemplate) {
                case 'main':
                    require_once(__DIR__."/templates/main_page.php");
                    break;
                case 'new':
                    require_once(__DIR__."/templates/add_one_line_view.php");
                    break;
            }
        }
}