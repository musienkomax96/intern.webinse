$("#add_container").hide();
$("#update_container").hide();

$("#add_item").click(function() {
    $("#update_container").hide();
    $("#add_container").show();
});
var err = false;

function valid(val) {
    var temp_first_name, temp_second_name, temp_email;
    err = false;
    $(".error").remove();

    switch (val) {
        case 'add':
            temp_first_name = "#add_first_name";
            temp_second_name = "#add_second_name";
            temp_email = "#add_email";
            break;
        case 'update':
            temp_first_name = "#update_first_name";
            temp_second_name = "#update_second_name";
            temp_email = "#update_email";
            break;
    }

    var first_name = $(temp_first_name).val();
    var second_name = $(temp_second_name).val();
    var email = $(temp_email).val();

    if (first_name.length <= 2) {
        $(temp_first_name).after('<span class="error">This field is required</span>');
        err = true;
    }
    if (second_name.length <= 2) {
        $(temp_second_name).after('<span class="error">This field is required</span>');
        err = true;
    }
    if (email.length < 1) {
        $(temp_email).after('<span class="error">This field is required</span>');
        err = true;
    } else {
        var regEx = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var validEmail = regEx.test(email);
        if (!validEmail) {
            $(temp_email).after('<span class="error">Enter a valid email</span>');
            err = true;
        }
    }
}

$(".btn_update").click(function() {
    $("#add_container").hide();
    window.idGlob = $(this).attr("id").substr(4);
    //console.log($(this));
    //console.log($("#tr_" + idGlob).children().eq(1).html());
    var form_data = $("#tr_" + idGlob).children().eq(1).html();
    $('#form_update input[name="first_name"]').val(form_data);
    form_data = $("#tr_" + idGlob).children().eq(2).html();
    $('#form_update input[name="second_name"]').val(form_data);
    form_data = $("#tr_" + idGlob).children().eq(3).html();
    $('#form_update input[name="email"]').val(form_data);
    $("#update_container").show();
});

$(".cancel").click(function() {
    $(this).parent().parent().parent().hide();
});

$(".submit_add").click(function(e) {
    e.preventDefault(); //to not reloaded;
    var formData = $("#form_add").serialize() + "&type=new"; //take values of form;
    //console.log(formData);

    valid("add");
    if (!err){
        $.ajax({
            type: "POST",
            url: "index.php",
            data: formData,
            success: function(data) {
                if (data != '0') {
                    $("table").append(data);
                    //clear form data
                    $('#form_add input[name="first_name"]').val("");
                    $('#form_add input[name="second_name"]').val("");
                    $('#form_add input[name="email"]').val("");
                }
            }
        });
    }


});

$(".btn_delete").click(function() {
    var id = $(this).attr("id").substr(4);
    var str = "type=delete&id=" + id;

    $.ajax({
        type: "POST",
        url: "index.php",
        data: str,
        success: function(data) {

            if (data != '0') {
                $('#tr_' + id).empty();
            }
        }
    });
});

$("#sub_btn_update").click(function(e) {

    e.preventDefault(); //to not reloaded;

    var formData = $("#form_update").serialize() + "&type=update&id=" + idGlob;
    //console.log(formData);
    valid("update");
    if (!err) {
        $.ajax({
            type: "POST",
            url: "/index.php",
            data: formData,
            success: function(data) {
                if (data != '0') {
                    var arrData = jQuery.parseJSON(data);
                    console.log(arrData);
                    $("#tr_" + idGlob).children().eq(1).html(arrData["first_name"]);
                    $("#tr_" + idGlob).children().eq(2).html(arrData["second_name"]);
                    $("#tr_" + idGlob).children().eq(3).html(arrData["email"]);

                    $('#form_update input[name="first_name"]').val("");
                    $('#form_update input[name="second_name"]').val("");
                    $('#form_update input[name="email"]').val("");

                    $("#update_container").hide();
                }
            }
        });
    }
});